﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    public class Saver
    {
        private static Saver instance;

        private Saver() { }

        public static Saver GetInstance()
        {
            if (instance == null)
            {
                instance = new Saver();
            }
            return instance;
        }

        private void Serialize(List<Matrix> matrices, string path)
        {
            var formatter = new BinaryFormatter();
            using (var stream = new FileStream(path, FileMode.Open))
            {
                formatter.Serialize(stream, matrices);
            }
        }

        private List<Matrix> Deserialize(string path)
        {
            var formatter = new BinaryFormatter();
            var matrices = new List<Matrix>();
            using (var stream = new FileStream(path, FileMode.Open))
            {
                if (stream.Length != 0)
                {
                    matrices = formatter.Deserialize(stream) as List<Matrix>;
                }
            }
            return matrices;
        }

        public void OpenOrCreateFile(string path)
        {
            using (var stream = new FileStream(path, FileMode.OpenOrCreate)) { }
        }

        public void WriteMatrix(string path, Matrix matrix)
        {
            var matrices = Deserialize(path);
            matrices.Add(matrix);
            Serialize(matrices, path);
        }

        public Matrix ReadMatrixById(string path, int id)
        {
            List<Matrix> matrices = ReadAllMatrices(path);

            if (matrices.Count < id || id <= 0)
            {
                return null;
            }

            return matrices.ElementAt(id - 1);
        }

        public void RewriteOrAppend(string path, int id, Matrix matrix)
        {
            var matrices = Deserialize(path);

            if (matrices.Count < id || id < 0)
            {
                return;
            }
            if (matrices[id].Rows < matrix.Rows || matrices[id].Cols < matrix.Cols)
            {
                matrices.Add(matrix);
            }
            else
            {
                matrices.Insert(id, matrix);
                matrices.RemoveAt(id + 1);
            }

            Serialize(matrices, path);
        }

        public void DeleteMatrix(string path, Matrix matrix)
        {
            List<Matrix> matrices = Deserialize(path);

            for (int i = 0; i < matrices.Count; i++)
            {
                if (matrices[i] == matrix)
                {
                    matrices.Remove(matrix);
                    break;
                }
            }

            Serialize(matrices, path);
        }

        public void DeleteMatrixById(string path, int id)
        {
            List<Matrix> matrices = Deserialize(path);

            if (matrices.Count < id || id <= 0)
            {
                return;
            }

            matrices.RemoveAt(id);

            Serialize(matrices, path);
        }

        public void ClearFile(string path)
        {
            File.WriteAllText(path, string.Empty);
        }

        public List<Matrix> ReadAllMatrices(string path)
        {
            return Deserialize(path);
        }
    }
}
