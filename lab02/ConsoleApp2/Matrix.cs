﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    [Serializable]
    public class Matrix
    {
        public int Rows;
        public int Cols;
        protected double[,] mas;
        static Random rnd;
        protected double Determinant;
        public Matrix(int rows, int cols)
        {
            Rows = rows;
            Cols = cols;
            rnd = new Random();
        }
        public Matrix()
        {
            Rows = 0;
            Cols = 0;
        }
        public void FillMatrix()
        {
            System.Threading.Thread.Sleep(20);
            mas = new double[Rows, Cols];
            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Cols; j++)
                {
                    mas[i, j] = rnd.Next(0, 10);
                }
            }
        }
        public void Print()
        {
            for (int i = 0; i < Rows; i++)
            {
                Console.Write("|");
                for (int j = 0; j < Cols; j++)
                {
                    Console.Write($"{mas[i, j],-10}");
                }
                Console.WriteLine("|");
            }
        }
        public static Matrix operator *(Matrix a, Matrix b)
        {
            Matrix result = new Matrix(a.Rows, b.Cols);
            result.mas = new double[a.Rows, b.Cols];
            if (a.Cols == b.Rows)
            {
               
                for (int i = 0; i < a.Rows; i++)
                {

                    for (int j = 0; j < b.Cols; j++)
                    {
                        result.mas[i, j] = 0;
                        for (int h = 0; h < a.Cols; h++)
                            result.mas[i, j] += a.mas[i, h] * b.mas[h, j];
                    }

                }
                return result;
            }
            else
            {
                for (int i = 0; i < a.Rows; i++)
                {

                    for (int j = 0; j < b.Cols; j++)
                    {
                        result.mas[i, j] = 0;
                    }
                }
                return result;
            }
        }
        public Matrix Trans()
        {
            int r, c;
            c = Rows;
            r = Cols;
            double[,] trans = new double[r, c];
            Matrix matrix = new Matrix(r, c);
            for (int i = 0; i < r; i++)
            {
                for (int j = 0; j < c; j++)
                {
                    trans[i, j] = mas[j, i];
                }
            }
            mas = new double[r, c];
            mas = trans;
            Rows = r;
            Cols = c;
            matrix.mas = mas;
            return matrix;
        }

        public double GetDeterminant()
        {
            if (Rows != Cols)
            {
                return 0;
            }
            return CalcDeterminant(mas);
        }

        private double CalcDeterminant(double[,] matrix)
        {
            int n = matrix.GetLength(0);

            if (n == 2)
            {
                return matrix[0, 0] * matrix[1, 1] - matrix[0, 1] * matrix[1, 0];
            }

            double[,] L = new double[n, n];
            double[,] U = new double[n, n];

            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    U[0, i] = matrix[0, i];
                    L[i, 0] = matrix[i, 0] / U[0, 0];
                    double sum = 0;
                    for (int k = 0; k < i; k++)
                    {
                        sum += L[i, k] * U[k, j];
                    }
                    U[i, j] = matrix[i, j] - sum;
                    if (i > j)
                    {
                        L[j, i] = 0;
                    }
                    else
                    {
                        sum = 0;
                        for (int k = 0; k < i; k++)
                        {
                            sum += L[j, k] * U[k, i];
                        }
                        L[j, i] = (matrix[j, i] - sum) / U[i, i];
                    }
                }
            }

            Determinant = 1;
            for (int i = 0; i < n; i++)
            {
                Determinant *= U[i, i];
            }
            return Determinant;
        }
        public Matrix InversedMatrix()
        {
            if (Rows != Cols)
            {
                return null;
            }

            double determinant = GetDeterminant();

            if (determinant == 0)
            {
                return null;
            }

            Matrix transposed = Trans();
            Matrix DetMat = new Matrix(Rows, Cols);
            DetMat.mas = new double[Rows, Cols];
            int size = mas.GetLength(0);
            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Cols; j++)
                {
                    double[,] lesserMatrix = new double[size - 1, size - 1];
                    int row = 0;
                    int col = 0;
                    for (int k = 0; k < size; k++)
                    {
                        for (int l = 0; l < size; l++)
                        {

                            if (k == i || l == j)
                            {
                                continue;
                            }

                            lesserMatrix[row, col] = transposed.mas[k, l];
                            col++;
                        }
                        if (col == lesserMatrix.GetLength(0))
                        {
                            row++;
                            col = 0;
                        }
                    }

                    DetMat.mas[i, j] = 1.0 * (Math.Pow(-1, i - j) * CalcDeterminant(lesserMatrix));
                }
            }
            double f = 1.0 * (1 / determinant);
            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Cols; j++)
                {
                    DetMat.mas[i, j] = Math.Round((f * DetMat.mas[i, j]), 2);
                }
            }
            mas = DetMat.mas;
            return DetMat;
        }
        ~Matrix() { }
    }
}