﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Program
    {
        public static string Path = "C:\\Users\\User\\Desktop\\text.txt";
        static void Main(string[] args)
        {
            var saver = Saver.GetInstance();
            saver.OpenOrCreateFile(Path);
            var matrices = new List<Matrix>();

            for (int i = 0; i < 5; i++)
            {
                var matrix = new Matrix(20, 20);
                matrix.FillMatrix();
                matrices.Add(matrix);
            }
            foreach (var matrix in matrices)
            {
                saver.WriteMatrix(Path, matrix);
            }

            for (int i = 0; i < matrices.Count; i++)
            {
                Console.WriteLine($"Визначник {i} матрицi: {matrices[i].GetDeterminant()}");
            }

            Matrix sum = matrices[0] * matrices[1];
            saver.WriteMatrix(Path, sum);

            for (int i = 2; i < matrices.Count; i++)
            {
                sum *= matrices[i];
                saver.WriteMatrix(Path, sum);
            }

            Console.WriteLine("Обернена матриця до матрицi, яка є добутком перших 5 матриць: ");
            sum.InversedMatrix().Print();

            for (int i = 0; i < 4; i++)
            {
                matrices[i] = new Matrix(5, 5);
                matrices[i].FillMatrix();
            }

            sum = matrices[0] * matrices[1];

            for (int i = 2; i < 5; i++)
            {
                sum *= matrices[i];
            }

            saver.WriteMatrix(Path, sum);
            saver.ReadAllMatrices(Path);           
            Console.WriteLine($"Кiлькiсть усiх матриць у файлi: {saver.ReadAllMatrices(Path).Count}");
            saver.ClearFile(Path);
        }
    }
}