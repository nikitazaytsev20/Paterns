﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.ServiceModel.Discovery;

namespace Server
{
    public partial class Form1 : Form
    {
        ServiceHost serviceHost;
        public Form1()
        {
            InitializeComponent();
        }

        private void buttonStart_Click(object sender, EventArgs e)
        {
            string hostName = Dns.GetHostName();
            IPAddress[] ips = Dns.GetHostByName(hostName).AddressList;
            string ip = ips[0].ToString();
            int port = 9000;
            string path = "calc";
            string url = $"net.tcp://{ip}:{port}/{path}";
            labelAddress.Text = "Точка підключення: " + url;
            serviceHost = new ServiceHost(typeof(Processor), new Uri(url));
            NetTcpBinding netTcpBinding = new NetTcpBinding();
            netTcpBinding.Security.Mode = SecurityMode.None;
            serviceHost.AddServiceEndpoint(
                typeof(IProcesses),netTcpBinding,""
            );
            serviceHost.Description.Behaviors.Add(new ServiceMetadataBehavior());
            serviceHost.AddServiceEndpoint(
                typeof(IMetadataExchange),
                MetadataExchangeBindings.CreateMexTcpBinding(),
                "mex"
            );
            serviceHost.AddServiceEndpoint(
                new UdpDiscoveryEndpoint()
            );
            ServiceDiscoveryBehavior serviceDiscoveryBehavior = new ServiceDiscoveryBehavior();
            serviceHost.Description.Behaviors.Add(serviceDiscoveryBehavior);
            try
            {
                serviceHost.Open();
                buttonStart.Enabled = false;
                buttonStop.Enabled = true;
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "Помилка старту сервера");
            }
        }

        private void buttonStop_Click(object sender, EventArgs e)
        {
            serviceHost.Close();
            buttonStart.Enabled = true;
            buttonStop.Enabled = false;
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
