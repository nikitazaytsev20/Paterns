﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Server
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    public class Processor : IProcesses
    {
        Form form = (Application.OpenForms["Form1"] as Form1).FindForm();
        Label label = (Application.OpenForms["Form1"] as Form1).labelMessage;
        void IProcesses.SendMessage(string message)
        {
            if (label.InvokeRequired)
            {
                label.Invoke(new Action(() =>
                {
                    label.Text = "Клієнт:" + message;
                }));
            }
            else label.Text = "Клієнт:" + message;
        }

        void IProcesses.StartProgram(string name)
        {
            Process process = new Process();
            process.StartInfo.FileName = name;
            process.Start();
            if (label.InvokeRequired)
            {
                label.Invoke(new Action(() =>
                {
                    label.Text = "Клієнт:" + $"{name} запущена";
                }));
            }
            else label.Text = "Клієнт:" + $"{name} запущена";
        }


        string[] IProcesses.GetProcesses()
        {
            Process[] processes = Process.GetProcesses();
            string[] strProcesses = new string[processes.Length];
            try
            {
                for (int i = 0; i < processes.Length; i++)
                {
                    try
                    {
                        strProcesses[i] = $"{processes[i].Id},{processes[i].ProcessName},{processes[i].Threads.Count},{processes[i].VirtualMemorySize64}";
                    }
                    catch
                    {
                        strProcesses[i] = null;
                    }
                }
                return strProcesses;
            }
            catch
            {
                return null;
            }
        }

        Bitmap IProcesses.ScreenShot()
        {
            Bitmap printscreen = new Bitmap(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height,
                System.Drawing.Imaging.PixelFormat.Format32bppArgb);
            form.Activate();
            Graphics graphics = Graphics.FromImage(printscreen);
            graphics.CopyFromScreen(0, 0, 0, 0, Screen.PrimaryScreen.Bounds.Size);
            return printscreen;
        }
    }
}
