﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
namespace Server
{
    [ServiceContract]
    public interface IProcesses
    {
        [OperationContract]
        void SendMessage(string message);
        [OperationContract]
        void StartProgram(string name);
        [OperationContract]
        Bitmap ScreenShot();
        [OperationContract]
        string []GetProcesses();

    }
}
