﻿using Client.SRProcess;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.ServiceModel.Discovery;
using System.Diagnostics;
using System.Text.RegularExpressions;

namespace Client
{
    public partial class Form1 : Form
    {
        ProcessesClient client;
        public Form1()
        {
            InitializeComponent();
        }

        private void buttonSend_Click(object sender, EventArgs e)
        {
            client.SendMessage(textBoxMessage.Text);

        }

        private void buttonConnect_Click(object sender, EventArgs e)
        {
            if (listBoxServers.Items.Count == 0 && listBoxServers.Items != null)
                return;
            string url = listBoxServers.SelectedItem.ToString();
            NetTcpBinding netTcpBinding = new NetTcpBinding();
            netTcpBinding.MaxReceivedMessageSize = 104857600;
            netTcpBinding.Security.Mode = SecurityMode.None;
            try
            {
                client = new ProcessesClient(
                    netTcpBinding,
                    new EndpointAddress(url)
                 );
                buttonConnect.Enabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Помилка");
            }
        }

        private void buttonDiscovery_Click(object sender, EventArgs e)
        {
            listBoxServers.Items.Clear();
            UdpDiscoveryEndpoint udpDiscoveryEndpoint = new UdpDiscoveryEndpoint();
            DiscoveryClient discoveryClient = new DiscoveryClient(udpDiscoveryEndpoint);
            FindCriteria findCriteria = new FindCriteria(typeof(IProcesses));
            findCriteria.Duration = new TimeSpan(0, 0, 1);
            FindResponse response = discoveryClient.Find(findCriteria);
            for (int i = 0; i < response.Endpoints.Count; i++)
            {
                listBoxServers.Items.Add(response.Endpoints[i].Address.ToString());
            }
        }

        private void buttonWord_Click(object sender, EventArgs e)
        {
            client.StartProgram("winword");
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void buttonScreenshot_Click(object sender, EventArgs e)
        {
            Bitmap bitmap = client.ScreenShot();
            SaveFileDialog dialog = new SaveFileDialog();
            dialog.Filter = "JPeg Image|*.jpg";
            dialog.DefaultExt = "printscreen";
            if (dialog.ShowDialog() == DialogResult.OK ) {
                bitmap.Save(dialog.FileName, System.Drawing.Imaging.ImageFormat.Jpeg);
            }

        }
        int[] ids;
        void displayProcesses()
        {
            string[] arr = client.GetProcesses();
            Match match;
            ids = new int[arr.Length];
            Regex regex = new Regex("(\\S+),(\\S+),(\\S+),(\\S+)", RegexOptions.Compiled);
            dataGridViewProcesses.RowCount = arr.Length;
            for (int i = 0; i < arr.Length; i++)
            {
                match = regex.Match(arr[i]);
                if (match.Success)
                {
                    ids[i] = int.Parse(match.Groups[1].Value);
                    dataGridViewProcesses[0, i].Value = match.Groups[1].Value;
                    dataGridViewProcesses[1, i].Value = match.Groups[2].Value;
                    dataGridViewProcesses[2, i].Value = match.Groups[3].Value;
                    dataGridViewProcesses[3, i].Value = match.Groups[4].Value;
                }
            }
        }
        private void button1_Click(object sender, EventArgs e)
        {
            displayProcesses();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            int selectedId = dataGridViewProcesses.SelectedRows[0].Index;
            Process process = Process.GetProcessById(ids[selectedId]);
            try
            {
                process.Kill();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                displayProcesses();
            }
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            client.StartProgram("calc");
        }
    }
}
