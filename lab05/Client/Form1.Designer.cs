﻿namespace Client
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonSend = new System.Windows.Forms.Button();
            this.buttonConnect = new System.Windows.Forms.Button();
            this.textBoxMessage = new System.Windows.Forms.TextBox();
            this.buttonDiscovery = new System.Windows.Forms.Button();
            this.listBoxServers = new System.Windows.Forms.ListBox();
            this.buttonWord = new System.Windows.Forms.Button();
            this.buttonScreenshot = new System.Windows.Forms.Button();
            this.buttonDisplayProcesses = new System.Windows.Forms.Button();
            this.buttonKill = new System.Windows.Forms.Button();
            this.dataGridViewProcesses = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.labelMessage = new System.Windows.Forms.Label();
            this.buttonCalc = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewProcesses)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonSend
            // 
            this.buttonSend.Location = new System.Drawing.Point(383, 156);
            this.buttonSend.Name = "buttonSend";
            this.buttonSend.Size = new System.Drawing.Size(97, 27);
            this.buttonSend.TabIndex = 0;
            this.buttonSend.Text = "Відправити";
            this.buttonSend.UseVisualStyleBackColor = true;
            this.buttonSend.Click += new System.EventHandler(this.buttonSend_Click);
            // 
            // buttonConnect
            // 
            this.buttonConnect.Location = new System.Drawing.Point(44, 67);
            this.buttonConnect.Name = "buttonConnect";
            this.buttonConnect.Size = new System.Drawing.Size(120, 40);
            this.buttonConnect.TabIndex = 2;
            this.buttonConnect.Text = "Під\'єднатися до вибраного сервера";
            this.buttonConnect.UseVisualStyleBackColor = true;
            this.buttonConnect.Click += new System.EventHandler(this.buttonConnect_Click);
            // 
            // textBoxMessage
            // 
            this.textBoxMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxMessage.Location = new System.Drawing.Point(35, 157);
            this.textBoxMessage.Name = "textBoxMessage";
            this.textBoxMessage.Size = new System.Drawing.Size(329, 26);
            this.textBoxMessage.TabIndex = 3;
            // 
            // buttonDiscovery
            // 
            this.buttonDiscovery.Location = new System.Drawing.Point(44, 12);
            this.buttonDiscovery.Name = "buttonDiscovery";
            this.buttonDiscovery.Size = new System.Drawing.Size(120, 34);
            this.buttonDiscovery.TabIndex = 4;
            this.buttonDiscovery.Text = "Знайти сервери";
            this.buttonDiscovery.UseVisualStyleBackColor = true;
            this.buttonDiscovery.Click += new System.EventHandler(this.buttonDiscovery_Click);
            // 
            // listBoxServers
            // 
            this.listBoxServers.FormattingEnabled = true;
            this.listBoxServers.Location = new System.Drawing.Point(187, 12);
            this.listBoxServers.Name = "listBoxServers";
            this.listBoxServers.Size = new System.Drawing.Size(210, 95);
            this.listBoxServers.TabIndex = 5;
            // 
            // buttonWord
            // 
            this.buttonWord.Location = new System.Drawing.Point(422, 13);
            this.buttonWord.Name = "buttonWord";
            this.buttonWord.Size = new System.Drawing.Size(87, 33);
            this.buttonWord.TabIndex = 6;
            this.buttonWord.Text = "Word";
            this.buttonWord.UseVisualStyleBackColor = true;
            this.buttonWord.Click += new System.EventHandler(this.buttonWord_Click);
            // 
            // buttonScreenshot
            // 
            this.buttonScreenshot.Location = new System.Drawing.Point(459, 67);
            this.buttonScreenshot.Name = "buttonScreenshot";
            this.buttonScreenshot.Size = new System.Drawing.Size(110, 33);
            this.buttonScreenshot.TabIndex = 7;
            this.buttonScreenshot.Text = "Зробити скриншот";
            this.buttonScreenshot.UseVisualStyleBackColor = true;
            this.buttonScreenshot.Click += new System.EventHandler(this.buttonScreenshot_Click);
            // 
            // buttonDisplayProcesses
            // 
            this.buttonDisplayProcesses.Location = new System.Drawing.Point(199, 348);
            this.buttonDisplayProcesses.Name = "buttonDisplayProcesses";
            this.buttonDisplayProcesses.Size = new System.Drawing.Size(95, 50);
            this.buttonDisplayProcesses.TabIndex = 9;
            this.buttonDisplayProcesses.Text = "Відобразити процеси";
            this.buttonDisplayProcesses.UseVisualStyleBackColor = true;
            this.buttonDisplayProcesses.Click += new System.EventHandler(this.button1_Click);
            // 
            // buttonKill
            // 
            this.buttonKill.Location = new System.Drawing.Point(371, 349);
            this.buttonKill.Name = "buttonKill";
            this.buttonKill.Size = new System.Drawing.Size(96, 49);
            this.buttonKill.TabIndex = 11;
            this.buttonKill.Text = "Зупинити вибраний процес";
            this.buttonKill.UseVisualStyleBackColor = true;
            this.buttonKill.Click += new System.EventHandler(this.button2_Click);
            // 
            // dataGridViewProcesses
            // 
            this.dataGridViewProcesses.AllowUserToAddRows = false;
            this.dataGridViewProcesses.AllowUserToDeleteRows = false;
            this.dataGridViewProcesses.AllowUserToResizeColumns = false;
            this.dataGridViewProcesses.AllowUserToResizeRows = false;
            this.dataGridViewProcesses.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridViewProcesses.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridViewProcesses.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewProcesses.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4});
            this.dataGridViewProcesses.Location = new System.Drawing.Point(35, 218);
            this.dataGridViewProcesses.Name = "dataGridViewProcesses";
            this.dataGridViewProcesses.ReadOnly = true;
            this.dataGridViewProcesses.RowHeadersVisible = false;
            this.dataGridViewProcesses.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewProcesses.Size = new System.Drawing.Size(560, 119);
            this.dataGridViewProcesses.TabIndex = 12;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Id";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 41;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Процеси";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 76;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Кількість потоків";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Width = 108;
            // 
            // Column4
            // 
            this.Column4.HeaderText = "Обсяг ОЗУ";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Width = 82;
            // 
            // labelMessage
            // 
            this.labelMessage.AutoSize = true;
            this.labelMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelMessage.Location = new System.Drawing.Point(40, 129);
            this.labelMessage.Name = "labelMessage";
            this.labelMessage.Size = new System.Drawing.Size(200, 20);
            this.labelMessage.TabIndex = 13;
            this.labelMessage.Text = "Введіть текст на сервер:";
            // 
            // buttonCalc
            // 
            this.buttonCalc.Location = new System.Drawing.Point(515, 13);
            this.buttonCalc.Name = "buttonCalc";
            this.buttonCalc.Size = new System.Drawing.Size(92, 33);
            this.buttonCalc.TabIndex = 14;
            this.buttonCalc.Text = "Калькулятор";
            this.buttonCalc.UseVisualStyleBackColor = true;
            this.buttonCalc.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(619, 409);
            this.Controls.Add(this.buttonCalc);
            this.Controls.Add(this.labelMessage);
            this.Controls.Add(this.dataGridViewProcesses);
            this.Controls.Add(this.buttonKill);
            this.Controls.Add(this.buttonDisplayProcesses);
            this.Controls.Add(this.buttonScreenshot);
            this.Controls.Add(this.buttonWord);
            this.Controls.Add(this.listBoxServers);
            this.Controls.Add(this.buttonDiscovery);
            this.Controls.Add(this.textBoxMessage);
            this.Controls.Add(this.buttonConnect);
            this.Controls.Add(this.buttonSend);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Client";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewProcesses)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonSend;
        private System.Windows.Forms.Button buttonConnect;
        private System.Windows.Forms.TextBox textBoxMessage;
        private System.Windows.Forms.Button buttonDiscovery;
        private System.Windows.Forms.ListBox listBoxServers;
        private System.Windows.Forms.Button buttonWord;
        private System.Windows.Forms.Button buttonScreenshot;
        private System.Windows.Forms.Button buttonDisplayProcesses;
        private System.Windows.Forms.Button buttonKill;
        private System.Windows.Forms.DataGridView dataGridViewProcesses;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.Label labelMessage;
        private System.Windows.Forms.Button buttonCalc;
    }
}

