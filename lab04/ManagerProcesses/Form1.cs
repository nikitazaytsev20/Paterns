﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ManagerProcesses
{
    public partial class Form1 : Form
    {
        Process program;
        Process[] processes;
        int[] arr;
        public Form1()
        {
            InitializeComponent();
        }

        private void comboBoxPrograms_SelectedIndexChanged(object sender, EventArgs e)
        {
            program = new Process();
            if (comboBoxPrograms.SelectedIndex == 0)
            {
                program.StartInfo.FileName = "winword";
                program.Start();
            }
            else if (comboBoxPrograms.SelectedIndex == 1)
            {
                program.StartInfo.FileName = "calc";
                program.Start();
            }
            else if (comboBoxPrograms.SelectedIndex == 2)
            {
                program.StartInfo.FileName = "POWERPNT";
                program.Start();
            }
            else if (comboBoxPrograms.SelectedIndex == 3)
            {
                program.StartInfo.FileName = "MSACCESS";
                program.Start();
            }
            else if (comboBoxPrograms.SelectedIndex == 4)
            {
                program.StartInfo.FileName = "chrome";
                program.Start();
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            dataGridViewProcesses.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            GetProcesses();
        }
        public void GetProcesses()
        {
            processes = Process.GetProcesses();
            arr = new int[processes.Length];
            dataGridViewProcesses.RowCount = processes.Length;
            for (int i = 0; i < processes.Length; i++)
            {
                arr[i] = processes[i].Id;
                dataGridViewProcesses[0, i].Value = processes[i].Id.ToString();
                dataGridViewProcesses[1, i].Value = processes[i].ProcessName;
                try
                {
                    dataGridViewProcesses[3, i].Value = processes[i].PriorityClass;
                }
                catch
                {
                    dataGridViewProcesses[3, i].Value = "Відмовлено в доступі";
                }
                dataGridViewProcesses[4, i].Value = processes[i].Threads.Count;
                dataGridViewProcesses[5, i].Value = processes[i].VirtualMemorySize64;
                try
                {
                    dataGridViewProcesses[2, i].Value = processes[i].StartTime;
                }
                catch
                {
                    dataGridViewProcesses[2, i].Value = "Невідомо";
                }      
            }
        }

        private void buttonGetProcess_Click(object sender, EventArgs e)
        {
            GetProcesses();
        }

        private void buttonKillProcess_Click(object sender, EventArgs e)
        {
            int selectedId = dataGridViewProcesses.SelectedRows[0].Index;
            Process process = Process.GetProcessById(arr[selectedId]);
            try
            {
                process.Kill();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                GetProcesses();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                int selectedId = dataGridViewProcesses.SelectedRows[0].Index;
                Process process = Process.GetProcessById(arr[selectedId]);
                if (comboBoxPriority.SelectedIndex == 0)
                    process.PriorityClass = ProcessPriorityClass.Idle;
                else if (comboBoxPriority.SelectedIndex == 1)
                    process.PriorityClass = ProcessPriorityClass.BelowNormal;
                else if (comboBoxPriority.SelectedIndex == 2)
                    process.PriorityClass = ProcessPriorityClass.Normal;
                else if (comboBoxPriority.SelectedIndex == 3)
                    process.PriorityClass = ProcessPriorityClass.AboveNormal;
                else if (comboBoxPriority.SelectedIndex == 4)
                    process.PriorityClass = ProcessPriorityClass.High;
                else if (comboBoxPriority.SelectedIndex == 5)
                    process.PriorityClass = ProcessPriorityClass.RealTime;
                else MessageBox.Show("Помилка");
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                GetProcesses();
            }
        }
    }
}
