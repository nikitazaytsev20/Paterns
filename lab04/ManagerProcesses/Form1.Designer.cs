﻿namespace ManagerProcesses
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.comboBoxPrograms = new System.Windows.Forms.ComboBox();
            this.labelPrograms = new System.Windows.Forms.Label();
            this.dataGridViewProcesses = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.buttonGetProcess = new System.Windows.Forms.Button();
            this.buttonKillProcess = new System.Windows.Forms.Button();
            this.buttonPriority = new System.Windows.Forms.Button();
            this.comboBoxPriority = new System.Windows.Forms.ComboBox();
            this.labelPriority = new System.Windows.Forms.Label();
            this.labelProcesses = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewProcesses)).BeginInit();
            this.SuspendLayout();
            // 
            // comboBoxPrograms
            // 
            this.comboBoxPrograms.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxPrograms.FormattingEnabled = true;
            this.comboBoxPrograms.Items.AddRange(new object[] {
            "Word",
            "Калькулятор",
            "PowerPoint",
            "Access",
            "Chrome"});
            this.comboBoxPrograms.Location = new System.Drawing.Point(615, 337);
            this.comboBoxPrograms.Name = "comboBoxPrograms";
            this.comboBoxPrograms.Size = new System.Drawing.Size(169, 21);
            this.comboBoxPrograms.TabIndex = 0;
            this.comboBoxPrograms.SelectedIndexChanged += new System.EventHandler(this.comboBoxPrograms_SelectedIndexChanged);
            // 
            // labelPrograms
            // 
            this.labelPrograms.AutoSize = true;
            this.labelPrograms.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelPrograms.Location = new System.Drawing.Point(611, 302);
            this.labelPrograms.Name = "labelPrograms";
            this.labelPrograms.Size = new System.Drawing.Size(173, 20);
            this.labelPrograms.TabIndex = 1;
            this.labelPrograms.Text = "Виберіть програму:";
            // 
            // dataGridViewProcesses
            // 
            this.dataGridViewProcesses.AllowUserToAddRows = false;
            this.dataGridViewProcesses.AllowUserToDeleteRows = false;
            this.dataGridViewProcesses.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridViewProcesses.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridViewProcesses.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewProcesses.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5,
            this.Column6});
            this.dataGridViewProcesses.Location = new System.Drawing.Point(18, 58);
            this.dataGridViewProcesses.Name = "dataGridViewProcesses";
            this.dataGridViewProcesses.ReadOnly = true;
            this.dataGridViewProcesses.RowHeadersVisible = false;
            this.dataGridViewProcesses.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            this.dataGridViewProcesses.Size = new System.Drawing.Size(788, 221);
            this.dataGridViewProcesses.TabIndex = 2;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Id";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 41;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Процеси";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 76;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Час запуску";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Width = 87;
            // 
            // Column4
            // 
            this.Column4.HeaderText = "Пріорітет процесу";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Width = 112;
            // 
            // Column5
            // 
            this.Column5.HeaderText = "Кількість потоків";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            this.Column5.Width = 108;
            // 
            // Column6
            // 
            this.Column6.HeaderText = "Обсяг ОЗУ";
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            this.Column6.Width = 82;
            // 
            // buttonGetProcess
            // 
            this.buttonGetProcess.Location = new System.Drawing.Point(36, 312);
            this.buttonGetProcess.Name = "buttonGetProcess";
            this.buttonGetProcess.Size = new System.Drawing.Size(83, 46);
            this.buttonGetProcess.TabIndex = 3;
            this.buttonGetProcess.Text = "Відобразити процеси";
            this.buttonGetProcess.UseVisualStyleBackColor = true;
            this.buttonGetProcess.Click += new System.EventHandler(this.buttonGetProcess_Click);
            // 
            // buttonKillProcess
            // 
            this.buttonKillProcess.Location = new System.Drawing.Point(138, 312);
            this.buttonKillProcess.Name = "buttonKillProcess";
            this.buttonKillProcess.Size = new System.Drawing.Size(103, 46);
            this.buttonKillProcess.TabIndex = 4;
            this.buttonKillProcess.Text = "Зупинити вибраний процес";
            this.buttonKillProcess.UseVisualStyleBackColor = true;
            this.buttonKillProcess.Click += new System.EventHandler(this.buttonKillProcess_Click);
            // 
            // buttonPriority
            // 
            this.buttonPriority.Location = new System.Drawing.Point(455, 312);
            this.buttonPriority.Name = "buttonPriority";
            this.buttonPriority.Size = new System.Drawing.Size(123, 46);
            this.buttonPriority.TabIndex = 5;
            this.buttonPriority.Text = "Змінити пріорітет вибраного процесу";
            this.buttonPriority.UseVisualStyleBackColor = true;
            this.buttonPriority.Click += new System.EventHandler(this.button1_Click);
            // 
            // comboBoxPriority
            // 
            this.comboBoxPriority.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxPriority.FormattingEnabled = true;
            this.comboBoxPriority.Items.AddRange(new object[] {
            "Низький",
            "Нижче середнього",
            "Середній",
            "Вище середнього",
            "Високий",
            "Найвищий"});
            this.comboBoxPriority.Location = new System.Drawing.Point(279, 337);
            this.comboBoxPriority.Name = "comboBoxPriority";
            this.comboBoxPriority.Size = new System.Drawing.Size(148, 21);
            this.comboBoxPriority.TabIndex = 6;
            // 
            // labelPriority
            // 
            this.labelPriority.AutoSize = true;
            this.labelPriority.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelPriority.Location = new System.Drawing.Point(276, 312);
            this.labelPriority.Name = "labelPriority";
            this.labelPriority.Size = new System.Drawing.Size(151, 16);
            this.labelPriority.TabIndex = 7;
            this.labelPriority.Text = "Виберіть пріорітет:";
            // 
            // labelProcesses
            // 
            this.labelProcesses.AutoSize = true;
            this.labelProcesses.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelProcesses.Location = new System.Drawing.Point(336, 22);
            this.labelProcesses.Name = "labelProcesses";
            this.labelProcesses.Size = new System.Drawing.Size(154, 20);
            this.labelProcesses.TabIndex = 1;
            this.labelProcesses.Text = "Виберіть процес:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(823, 396);
            this.Controls.Add(this.labelPriority);
            this.Controls.Add(this.comboBoxPriority);
            this.Controls.Add(this.buttonPriority);
            this.Controls.Add(this.buttonKillProcess);
            this.Controls.Add(this.buttonGetProcess);
            this.Controls.Add(this.dataGridViewProcesses);
            this.Controls.Add(this.labelProcesses);
            this.Controls.Add(this.labelPrograms);
            this.Controls.Add(this.comboBoxPrograms);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Менеджер процесів";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewProcesses)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBoxPrograms;
        private System.Windows.Forms.Label labelPrograms;
        private System.Windows.Forms.DataGridView dataGridViewProcesses;
        private System.Windows.Forms.Button buttonGetProcess;
        private System.Windows.Forms.Button buttonKillProcess;
        private System.Windows.Forms.Button buttonPriority;
        private System.Windows.Forms.ComboBox comboBoxPriority;
        private System.Windows.Forms.Label labelPriority;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.Label labelProcesses;
    }
}

