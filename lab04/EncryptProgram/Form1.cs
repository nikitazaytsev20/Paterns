﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EncryptProgram
{
    public partial class Form1 : Form
    {
        Stopwatch time = new Stopwatch();
        public Form1()
        {
            InitializeComponent();
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            this.Height = 240;
        }

        private void buttonFile_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Multiselect = false;
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                textBoxPath.Text = dialog.FileName;
            }
        }
        void Encrypt()
        {
            using (var fstream = new FileStream(textBoxPath.Text + ".crypt", FileMode.OpenOrCreate)) { }

            long allBytes = 0;
            var bytes = 0;
            var lastProgress = 0;
            int key = int.Parse(textBoxKey.Text);
            using (var fstreamRead = File.OpenRead(textBoxPath.Text))
            {
                using (var fstreamWrite = new FileStream(textBoxPath.Text + ".crypt", FileMode.Append))
                {
                    while (fstreamRead.Length != fstreamRead.Position)
                    {
                        byte[] portion = new byte[1024];

                        bytes = fstreamRead.Read(portion, 0, portion.Length);
                        if (portion.Length != bytes)
                        {
                            Array.Resize(ref portion, bytes);
                        }

                        int progress = (int)Math.Round(allBytes * 100.0 / fstreamRead.Length);

                        for (int i = 0; i < portion.Length; i++)
                        {
                            portion[i] = (byte)(portion[i] ^ key);
                            if (backgroundWorker.WorkerReportsProgress && progress != lastProgress)
                            {
                                backgroundWorker.ReportProgress(progress);
                                System.Threading.Thread.Sleep(1);
                                lastProgress = progress;
                            }
                        }

                        allBytes += portion.Length;

                        fstreamWrite.Write(portion, 0, portion.Length);
                    }
                }
            }
        }
        void Decrypt()
        {
            var allBytes = 0;
            var bytes = 0;
            var lastProgress = 0;
            int key = int.Parse(textBoxKey.Text);
            using (var fstreamRead = File.OpenRead(textBoxPath.Text))
            {
                using (var fstreamWrite = new FileStream(textBoxPath.Text.Replace(".crypt", ""), FileMode.Append))
                {
                    while (fstreamRead.Length != fstreamRead.Position)
                    {
                        byte[] portion = new byte[1024];

                        bytes = fstreamRead.Read(portion, 0, portion.Length);
                        if (portion.Length != bytes)
                        {
                            Array.Resize(ref portion, bytes);
                        }

                        var progress = (int)Math.Round(allBytes * 100.0 / fstreamRead.Length);

                        for (int i = 0; i < portion.Length; i++)
                        {
                            portion[i] = (byte)(portion[i] ^ key);

                            if (backgroundWorker.WorkerReportsProgress && progress != lastProgress)
                            {
                                backgroundWorker.ReportProgress(progress);
                                System.Threading.Thread.Sleep(1);
                                lastProgress = progress;
                            }
                        }

                        allBytes += portion.Length;
                        fstreamWrite.Write(portion, 0, portion.Length);
                    }
                }
            }

            if (backgroundWorker != null && backgroundWorker.WorkerReportsProgress)
            {
                backgroundWorker.ReportProgress(100);
            }
        }
        void myalgo()
        {
            if (!File.Exists(textBoxPath.Text))
            {
                MessageBox.Show("Шлях невірний!", "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }


            if (radioButtonEncrypt.Checked)
            {
                Encrypt();
            }

            if (radioButtonDecrypt.Checked)
            {
                Decrypt();
            }
        }

        private void buttonStart_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(textBoxPath.Text))
            {
                MessageBox.Show("Файл не вибрано!", "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (String.IsNullOrEmpty(textBoxKey.Text))
                MessageBox.Show("Ключ не введено! Введіть, будь ласка, ключ.", "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            else if (radioButtonEncrypt.Checked == false && radioButtonDecrypt.Checked == false)
            {
                MessageBox.Show("Не вибрано дію! Виберіть, будь ласка, дію.", "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                this.Height = 340;
                backgroundWorker.RunWorkerAsync();
                labelMemKey.Text = $"Ключ для минулої дії = {textBoxKey.Text}";
            }
        }
        private void backgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            time.Restart();
            myalgo();
        }

        private void backgroundWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            try
            {
                progressBar.Value = e.ProgressPercentage;
            }
            catch { 

            }
            finally
            {
                labelPercent.Text = "Відсотки:" + progressBar.Value.ToString() + "%";
            }
        }

        private void backgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            time.Stop();
            labelPercent.Text = "Відсотки: 100%";
            MessageBox.Show($"Виконано!\n" + $"Витрачено часу: {time.Elapsed}", "Успішно виконано", MessageBoxButtons.OK, MessageBoxIcon.Information);
            Thread.Sleep(3000);
            this.Height = 240;
        }
    }
}