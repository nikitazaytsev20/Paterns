﻿using Microsoft.VisualBasic.Devices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace ConsoleApp
{
    class Program
    {
        delegate void GetMessage();
        static void Main(string[] args)
        {
            int n;
            bool f = true;
            GetMessage del;
            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;
            Console.Title = "Назва автомобіля";
            string name = "Ford";
            del = New;
            do
            {
                Console.WriteLine($"Назва автомобіля - {name}");
                Console.Write("Введіть назву автомобіля - ");
                string newname = Console.ReadLine();
                do
                {
                    Console.Write("Змінити назву автомобіля? <так - клавіша <Y>, ні - клавіша <N>>");
                    f = true;
                    string flag = Console.ReadLine();
                    switch (flag)
                    {
                        case "y":
                        case "Y":
                            del.Invoke();
                            name = newname;
                            break;
                        case "n":
                        case "N":
                            del = Old;
                            del.Invoke();
                            break;
                        default:
                            Console.WriteLine("Введено невірне значення!"); f = false;
                            break;
                    }
                } while (f == false);
                Console.WriteLine($"Нова назва автомобіля - {name}\nВведіть 1, щоб продовжити або 0, щоб закінчити роботу.");
                n = int.Parse(Console.ReadLine());
            } while (n != 0);
        }
        private static void New()
        {
            Console.WriteLine($"\nНазва змінилася\n");
        }
        private static void Old()
        {
            Console.WriteLine("\nНазва не змінилася\n");
        }
    }
}